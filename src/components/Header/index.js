import * as S from './styles'

const Header = () => {
    return (
        <S.StyledHeader>
            <S.StyledLogo to="/">Filmes</S.StyledLogo>
        </S.StyledHeader>
    )
}

export default Header;