import styled from 'styled-components';
import { Link } from 'react-router-dom'

export const StyledHeader = styled.header`
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 100%;
  height: 60px;
  background-color: #000;
`;

export const StyledLogo = styled(Link)`
  text-decoration: none;
  font-size: 30px;
  cursor: pointer;
  color: #fff;
  font-weight: bold;
`;