export const setSearchResults = (results) => ({
    type: 'SET_SEARCH_RESULTS',
    payload: results,
});

export const clearSearchResults = () => ({
    type: 'CLEAR_SEARCH_RESULTS',
});