import { useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { setSearchResults, clearSearchResults } from '../../actions/searchActions';
import api from '../../services/api';
import * as S from './styles';
import { Card, Input } from 'antd';

const { Meta } = Card;
const { Search } = Input;

const Home = () => {
    const dispatch = useDispatch();
    const searchResults = useSelector((state) => state.search.searchResults);

    const [loading, setLoading] = useState(false);

    const onSearch = async (searchText) => {
        setLoading(true);

        try {
            const res = await api.get(`API/Search/k_0q52d96o/${searchText}`);
            dispatch(setSearchResults(res.data.results));
        } catch (error) {
            console.error("Erro ao buscar filmes:", error);
            dispatch(clearSearchResults());
        } finally {
            setLoading(false);
        }
    };

    if (loading) {
        return (
            <S.Loading>
                <h2>Carregando...</h2>
            </S.Loading>
        );
    }

    return (
        <S.Container>
            <S.MovieHeader>
                <Search
                    placeholder="Pesquise o filme aqui"
                    allowClear
                    disabled={loading}
                    onSearch={onSearch}
                    style={{ width: '300px' }}
                />
            </S.MovieHeader>
            {loading ? (
                <S.Loading>
                    <h2>Carregando...</h2>
                </S.Loading>
            ) : searchResults.length === 0 ? (
                <S.MovieInfo>Descubra o seu filme favorito!</S.MovieInfo>
            ) : (
                <S.MovieList>
                    {searchResults.map((movie) => (
                        <S.MovieLink key={movie.id} to={`/movie/${movie.id}`}>
                            <Card
                                style={{ width: 300 }}
                                cover={
                                    <img
                                        alt={movie.title}
                                        src={movie.image}
                                    />
                                }
                            >
                                <Meta title={movie.title} description={movie.description} />
                            </Card>
                        </S.MovieLink>
                    ))}
                </S.MovieList>

            )}

        </S.Container>
    );
};

export default Home;
