import styled from 'styled-components';
import { Link } from 'react-router-dom'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 14px;
`;

export const MovieHeader = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 14px;
`;

export const MovieInfo = styled.p`
    margin: 14px 0;
`;

export const MovieList = styled.div`
    margin: 14px auto;
    display: grid;
    grid-gap: 10px;
    grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));

    @media (min-width: 600px) {
        grid-template-columns: repeat(2, 1fr);
    }

    @media (min-width: 900px) {
        grid-template-columns: repeat(3, 1fr);
    }
`;

export const MovieLink = styled(Link)`
    width: 100%;
    cursor: pointer;
    transition: transform 0.3s ease-in-out;
    background-color: #fff;
    padding: 15px;
    text-decoration: none;
    border-radius: 4px;

    &:hover {
        transform: scale(1.05);
    }
`;

export const MovieTitle = styled.strong`
    margin-bottom: 14px;
    text-align: center;
    font-size: 22px;
    display: block;
`;

export const MovieImage = styled.img`
    width: 900px;
    max-width: 100%;
    max-height: 340px;
    object-fit: cover;
    object-position: center;
    display: block;
    border-radius: 8px 8px 0 0;
`;

export const Loading = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 14px;
`;