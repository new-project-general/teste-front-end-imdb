import styled from 'styled-components';
import { Link } from 'react-router-dom'

export const NotFoundContainer = styled.div`
    width: 100%;
    height: calc(100vh - 60px);
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

export const NotFoundTitle = styled.h1`
    font-size: 120px;
`;

export const NotFoundLink = styled(Link)`
    text-decoration: none;
    background-color: #116feb;
    color: #fff;
    padding: 10px;
    border-radius: 4px;
    margin-top: 25px;
`;