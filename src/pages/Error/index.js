import * as S from './styles'

const Error = () => {
    return(
        <S.NotFoundContainer>
            <S.NotFoundTitle>404</S.NotFoundTitle>
            <h2>Página não encontrada!</h2>
            <S.NotFoundLink  to="/">Verifique nossa lista de filmes</S.NotFoundLink >
        </S.NotFoundContainer>
    )
}

export default Error;