import { useEffect, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import api from '../../services/api'
import * as S from './styles'
import { Avatar, List } from 'antd';

const Movie = () => {
    const { id } = useParams()
    const navigate = useNavigate()

    const [movie, setMovie] = useState({})
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        async function loadMovie() {
            await api.get(`en/API/Title/k_0q52d96o/${id}`)
                .then((res) => {
                    setMovie(res.data)
                    setLoading(false)
                })
                .catch(() => {
                    navigate("/", { replace: true })
                    return
                })
        }

        loadMovie()

        return () => {
            console.log("desmontado")
        }
    }, [navigate, id])

    if (loading) {
        return (
            <S.MovieInfo>
                <h1>Carregando...</h1>
            </S.MovieInfo>
        )
    }


    return (
        <S.MovieInfo>
            <S.BackLink to={`/`}><i class="bi bi-arrow-left"></i> Home</S.BackLink >
            <S.MovieTitle>{movie.fullTitle}</S.MovieTitle>
            <S.MovieImage src={movie.image} alt={movie.fullTitle} />

            <S.MovieSectionTitle>Descrição</S.MovieSectionTitle>
            {/* <S.MovieDescription>{movie.year}</S.MovieDescription> */}

            <S.MovieSectionTitle>Ano de publicação</S.MovieSectionTitle>
            <S.MovieDescription>{movie.year}</S.MovieDescription>

            <S.MovieSectionTitle>O enredo do filme (plot)</S.MovieSectionTitle>
            <S.MovieDescription>{movie.plot}</S.MovieDescription>

            <S.MovieSectionTitle>Lista de atores</S.MovieSectionTitle>
            <List
                itemLayout="horizontal"
                dataSource={movie.actorList}
                renderItem={(item) => (
                    <List.Item>
                        <List.Item.Meta
                            avatar={<Avatar src={item.image} />}
                            title={item.name}
                            description={item.asCharacter}
                        />
                    </List.Item>
                )}
            />
        </S.MovieInfo>
    )
}

export default Movie