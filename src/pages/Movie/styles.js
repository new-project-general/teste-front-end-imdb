import styled from 'styled-components';
import { Link } from 'react-router-dom'

export const MovieInfo = styled.div`
    margin-top: 18px;
    display: flex;
    flex-direction: column;
    max-width: 800px;
    padding: 0 8px;
`;

export const MovieTitle = styled.h1`
    margin: 14px 0;
`;

export const MovieImage = styled.img`
    border-radius: 8px;
    width: 800px;
    max-width: 100%;
    max-height: 340px;
    object-fit: cover;
`;

export const MovieSectionTitle = styled.h3`
    margin-top: 14px;
`;

export const MovieDescription = styled.span`
    margin: 8px 0;
`;

export const BackLink = styled(Link)`
    color: #000;
    text-decoration: none;
    line-height: 100%;

    i {
        color: black;
        font-size: 20px;
        font-weight: bolder;
    }
`;

export const ButtonsContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const Button = styled.button`
    margin-right: 12px;
    margin-top: 14px;
    margin-left: 0;
    font-size: 20px;
    border: none;
    outline: none;
    padding: 12px;
    border-radius: 4px;
    cursor: pointer;
    transition: all .5s;
    background-color: #000;
    color: #fff;

    &:hover {
        background-color: brown;
        color: #fff;
    }
`;

export const StyledLink = styled(Link)`
    text-decoration: none;
`;